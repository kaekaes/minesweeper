﻿/* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Pixeye.Unity; //Foldouts

public class GameManager: MonoBehaviour {

	#region Variables
	[Foldout("Variables", true)]
		public int Tabx, Taby = 8;
		public int Percent = 10;
		public GameObject Casilla, Tablero;
		public GameObject Win, Lose;
		public bool Playing = false;
		public float timeFade = 0.05f;
		public Vector2 tileOutlineSize;
		public int BombasEnTotal;
	[Foldout("Listas", true)]
		public Sprite[] imagenes = new Sprite[11];
		public List<GameObject> tiles;
	#endregion

	#region UnityMethods
	public void StartGame() {
		Playing = true;
		if (Playing) {
			StartCoroutine(CreateTable());
		}
	}

	public void Turn(GameObject g, bool WantToCheck) {
		#region CheckNearTiles
		int n = int.Parse(g.name);
		//tile.GetComponent<Tile>().fakeClick();
		if (g.GetComponent<Tile>().bombsNear == 0 && WantToCheck) {
			if (n < Tabx) {
				if (n == 0) { // Esquina superior izquierda
					//Debug.Log("Esquina superior izquierda");
					if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
						tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
						tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
						tiles[n + (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
					}

				}else if (n == (Tabx - 1)) { // Esquina superior derecha
					//Debug.Log("Esq sup der");
					if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
						tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
						tiles[n + (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();

					}
					if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
						tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();

					}
				}else { // Zona superior
					//Debug.Log("zn sup");
					if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
						tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
						tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
						tiles[n + (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
						tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
						tiles[n + (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
				}
			}else if (n > ((Tabx*Taby)-Tabx)-1) { //Ultima columna
				if (n == (Tabx*Taby)-Tabx) { // Esquina inferior izquierda
					//Debug.Log("esqu inf iz");
					if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
						tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
						tiles[n - (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
						tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
					}

				}else if (n == (Tabx*Taby)-1) { // Esquina inferior derecha
					//Debug.Log("esq inf der");
					if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
						tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
						tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
						tiles[n - (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
				}else { // Zona inferior
					//Debug.Log("zn inf");
					if (!tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
						tiles[n - (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
						tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
						tiles[n - (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
						tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
					}
					if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
						tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
					}
				}
			}else if (n % Tabx == 0 && n != 0 && n != ((Tabx*Taby)-Tabx)) { // Zona lateral izquierdo
				//Debug.Log("zn lat iz");
				if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
					tiles[n - (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
					tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
					tiles[n + (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
			}else if (n % Tabx == (Tabx - 1) && n != (Tabx - 1) && n != ((Tabx * Taby) - 1)) { // Zona lateral derecho
				//Debug.Log("zn lat der");
				if (!tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
					tiles[n - (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
					tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
					tiles[n + (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
			}else { // zona central
				//Debug.Log("centro");
				if (!tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
					tiles[n - (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					tiles[n - Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
					tiles[n - (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n - 1].GetComponent<Tile>().imABomb) {
					tiles[n - 1].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + 1].GetComponent<Tile>().imABomb) {
					tiles[n + 1].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
					tiles[n + (Tabx - 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					tiles[n + Tabx].gameObject.GetComponent<Tile>().FakeClick();
				}
				if (!tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
					tiles[n + (Tabx + 1)].gameObject.GetComponent<Tile>().FakeClick();
				}
			}
		}
		#endregion

		#region TestForWin
		int aciertos = 0;
		int erroneas = 0;

		foreach (var tile in tiles){
			if (!tile.GetComponent<Tile>().imABomb && tile.GetComponent<Tile>().tengoBandera&&!tile.GetComponent<Tile>().imClicked) { erroneas++; }
			if (tile.GetComponent<Tile>().imABomb&& tile.GetComponent<Tile>().tengoBandera) { aciertos++; }

		}

		Debug.Log("Comprobación de victoria: " + aciertos + " " + erroneas + " " + BombasEnTotal);

		if ((erroneas == 0)&&(BombasEnTotal==aciertos)) {
			WinGame();
		}
		#endregion
	}
	IEnumerator CreateTable() {
		ReescaladoDeTiles();
		int name = 0;
		BombasEnTotal = 0;
		for (int x = 0; x < Tabx; x++) {
			for (int y = 0; y < Taby; y++) {
				if (Playing) {

					float t = (8 * 0.02f) / Tabx;
					yield return new WaitForSeconds(t);

					//Debug.Log(t);
					GameObject i = (GameObject)Instantiate(Casilla);
					tiles.Add(i);

					i.transform.SetParent(Tablero.transform);
					i.GetComponent<Tile>().gameManager = this;

					i.name = name.ToString();
					i.GetComponent<Tile>().id = name;
					i.GetComponent<Tile>().tileOutlineSize = tileOutlineSize;

					i.GetComponent<Button>().interactable = false;

					name++;

					int rng = Random.Range(1, 101);
					if (rng <= Percent) {
						i.GetComponent<Tile>().imABomb = true;
						BombasEnTotal++;
					}
				}
			}
		}
		float BombasPorHaber = (Tabx * Taby) * (Percent / 100.0f);
		float aux = BombasPorHaber - (int)BombasPorHaber;
		Debug.Log(BombasEnTotal + " " + BombasPorHaber.ToString() + " " + aux.ToString());

		if (aux >= 0.0f) {
			BombasPorHaber++;
		}


		if (BombasEnTotal < (int)BombasPorHaber) {
			while (BombasEnTotal < (int)BombasPorHaber) { 
				int rng = Random.Range(0, Tabx * Taby);
				if (tiles[rng].GetComponent<Tile>().imABomb) {
					continue;
				}
				tiles[rng].GetComponent<Tile>().imABomb = true;
				BombasEnTotal++;
				Debug.Log("Nueva bomba creada!");
			}
		}
		foreach (var i in tiles) { i.GetComponent<Button>().interactable = true; }
		foreach (var i in tiles) { i.GetComponent<Tile>().CheckBombs(); }
	}

	public void LoseGame(GameObject g) {
		Playing = false;
		foreach (var tile in tiles) {
			tile.GetComponent<Button>().interactable = false;
			if (tile.GetComponent<Tile>().imABomb && (tile!=g)) {
				tile.GetComponent<Image>().sprite = imagenes[0];
			}
		}
		Lose.SetActive(true);
	}
	public void WinGame() {
		foreach (var tile in tiles) {
			if (!tile.GetComponent<Tile>().imABomb) {
				tile.GetComponent<Tile>().FakeClick();
			}
			tile.GetComponent<Button>().interactable = false;
		}
		Playing = false;
		Win.SetActive(true);
		
	}

	public void Retry() {
		Playing = false;
		StopAllCoroutines();
		StartCoroutine(RetryWithTime());
	}

	public IEnumerator RetryWithTime() {
		foreach (var i in tiles) {
			yield return new WaitForSeconds(timeFade/1.5f);
			Destroy(i);
		}
		tiles.Clear();
		Playing = true;

		StartCoroutine(CreateTable());
		yield return new WaitForSeconds(timeFade * (Tabx * Taby));

	}

	public int CheckForBombs(GameObject g) {
		int BombasCercanas = 0;
		int n = int.Parse(g.name);
		// Primera columna
		if (n < Tabx) {
			if (n == 0) { // Esquina superior izquierda
				if (tiles[n + 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}

			} else if (n == (Tabx-1)) { // Esquina superior derecha
				if (tiles[n - 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
			} else { // Zona superior
				if (tiles[n - 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
			}
		} else if (n > ((Tabx * Taby) - Tabx) - 1) { //Ultima columna
			if (n == ((Tabx * Taby) - Tabx)) { // Esquina inferior izquierda
				if (tiles[n + 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}

			} else if (n == (Tabx*Taby)-1) { // Esquina inferior derecha
				if (tiles[n - 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
			} else { // Zona inferior
				if (tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n - 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
				if (tiles[n + 1].GetComponent<Tile>().imABomb) {
					BombasCercanas++;
				}
			}
		} else if (n % Tabx == 0 && n != 0 && n != ((Tabx * Taby) - Tabx)) { // Zona lateral izquierdo
			if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + 1].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
		} else if (n % Tabx == (Tabx - 1) && n != Tabx - 1 && n != (Tabx * Taby) - 1) { // Zona lateral derecho
			if (tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - 1].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
		} else { // zona central
			if (tiles[n - (Tabx + 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - (Tabx - 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n - 1].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + 1].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + (Tabx - 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + Tabx].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
			if (tiles[n + (Tabx + 1)].GetComponent<Tile>().imABomb) {
				BombasCercanas++;
			}
		}
		return BombasCercanas;
	}
	#region Menu
	public void ExitGame() {
		Application.Quit();
	}

	public void Easy() {
		Tabx = 8;
		Taby = 8;
		Percent = Random.Range(10,21);
	}
	public void Medium() {
		Tabx = 12;
		Taby = 12;
		Percent = Random.Range(15, 21);
	}
	public void Hard() {
		Tabx = 16;
		Taby = 16;
		Percent = Random.Range(15, 21);
	}

	public void ReescaladoDeTiles() {
		GridLayoutGroup gl = Tablero.GetComponent<GridLayoutGroup>();

		Vector2 cellsize = new Vector2(((8.0f * 100.0f) / Tabx),( (8.0f * 100.0f) / Taby));
		
		gl.cellSize = cellsize;

		Vector2 spacing = new Vector2((8.0f * 4.0f) / Tabx, (8.0f * 4.0f) / Taby);
		gl.spacing = spacing;

		tileOutlineSize = spacing;

		int padd = (8 * 2) / Tabx;
		gl.padding.left = gl.padding.top = padd;
	}

	public void DeleteGame() {
		Playing = false;
		foreach (var i in tiles) {
			Destroy(i);
		}
		tiles.Clear();
	}

	public void SetTabxy(string s) {
		Tabx = Taby = int.Parse(s);
	}
	public void SetBombPercentage(string s) {
		Percent = int.Parse(s);
	}

	public void CustomDiff() {
		InputField[] inputFields = FindObjectsOfType<InputField>();
		foreach (InputField i in inputFields) {
			if (i.text == "") {
				if (i.gameObject.name == "InputFieldBombs") {
					i.text = "10";
				}else if (i.gameObject.name == "InputFieldRows") {
					i.text = "8";
				}
			}
			string aux = i.text;
			i.text = "1";
			i.text = aux;
			
		}
	}
	#endregion
	#endregion

}
